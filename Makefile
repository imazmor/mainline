CPP = cpp

all: maquinaz/MaquinaZ.java

clean:
	rm -f maquinaz/MaquinaZ.java

maquinaz/MaquinaZ.java: maquinaz/MaquinaZ.java.in
	$(CPP) -x c -C -E -P -o maquinaz/MaquinaZ.java maquinaz/MaquinaZ.java.in
