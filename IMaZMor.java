// IMaZMor
// Int�rprete de M�quina-Z de Morgul, para dispositivos con J2ME
//
// Copyright (C) 2004-2010 Jos� Manuel Ferrer Ortiz (jmfo1982_arroba_gmail_com)
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; version 2 of the License.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
// Place, Suite 330, Boston, MA 02111-1307 USA; or visit the following URL:
// http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

import javax.microedition.lcdui.*;
import javax.microedition.midlet.*;

import maquinaz.*;


public class IMaZMor extends MIDlet implements CommandListener
{
  private Command    com_Aceptar;
  private Command    com_Salir;
  private Display    visualizador;
  private Form       formulario;
  private MaquinaZ   maquina;
  private StringItem texto;
  private TextField  entrada;


  // Constructor
  public IMaZMor ()
  {
    formulario = new Form       ("IMaZMor");
    texto      = new StringItem (null, "");
    entrada    = new TextField  (null, "", 128, TextField.ANY);

    formulario.append (texto);
    formulario.append (entrada);

    com_Aceptar = new Command ("Aceptar", Command.OK, 1);
    com_Salir   = new Command ("Salir", Command.EXIT, 2);
    formulario.addCommand (com_Salir);
    formulario.setCommandListener (this);

    visualizador = Display.getDisplay (this);
  }

  public void startApp ()
  {
    visualizador.setCurrent (formulario);

    // Creamos y ejecutamos la M�quina Z
    maquina = new MaquinaZ (com_Aceptar, visualizador, formulario, this, texto,
                            entrada);
    maquina.start();
  }

  public void pauseApp ()
  {
  }

  public void destroyApp (boolean incondicional)
  {
    maquina.error = true;
    maquina = null;
    notifyDestroyed();
  }

  public void commandAction (Command com, Displayable vis)
  {
    if (com == com_Salir)
      destroyApp (true);
    else  // com == com_Aceptar
    {
      // Eliminamos el comando Aceptar
      formulario.removeCommand (com_Aceptar);
      // Avisamos al hilo de la m�quina para que deje de esperar
      maquina.esperar = false;
    }
  }
}
